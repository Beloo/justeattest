package com.example.beloo.justeattest.presentation.restaurant

import android.location.Location
import com.example.beloo.justeattest.data.repository.restaurant.RestaurantsRepository
import com.example.beloo.justeattest.domain.postcode.LocationUseCase
import com.example.beloo.justeattest.domain.postcode.PostCodeUseCase
import com.example.beloo.justeattest.presentation.SubscriptionCache
import com.example.beloo.justeattest.presentation.SubscriptionCacheImpl
import com.karumi.dexter.DexterBuilder
import com.karumi.dexter.listener.PermissionGrantedResponse
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class RestaurantsPresenterTest {

    private lateinit var testable: RestaurantsContract.Presenter

    @Mock
    lateinit var repository: RestaurantsRepository
    @Mock
    lateinit var locationUseCase: LocationUseCase
    lateinit var subscriptionCache: SubscriptionCache
    @Mock
    lateinit var postCodeUseCase: PostCodeUseCase
    @Mock
    lateinit var locationPermission: Lazy<DexterBuilder>
    @Mock
    lateinit var dexter: DexterBuilder
    private val scheduler: Scheduler = Schedulers.trampoline()
    @Mock
    lateinit var location: Location
    @Mock
    lateinit var view: RestaurantsContract.View

    private val postCode = "se19"

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        subscriptionCache = SubscriptionCacheImpl(scheduler)
        testable = RestaurantsPresenter(
            repository, locationUseCase,
            postCodeUseCase, locationPermission, subscriptionCache)
        testable.bindView(view)
        `when`(location.latitude).thenReturn(1.0)
        `when`(location.longitude).thenReturn(2.0)
        `when`(locationPermission.get()).thenReturn(dexter)
        `when`(dexter.check()).thenAnswer {
            testable.onPermissionGranted(Mockito.mock(PermissionGrantedResponse::class.java))
        }
        `when`(locationUseCase.getLocation()).thenReturn(Single.just(location))
        `when`(postCodeUseCase.getPostCodeFromLocation(1.0, 2.0))
            .thenReturn(Single.just(postCode))
    }

    @Test
    fun findUserLocation_HasPermission_PostcodeSetToView() {
        testable.findUserLocation()
        verify(view).setPostCode(postCode)
    }
}