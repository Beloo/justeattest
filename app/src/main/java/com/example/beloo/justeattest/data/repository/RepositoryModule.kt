package com.example.beloo.justeattest.data.repository

import com.example.beloo.justeattest.data.repository.restaurant.RestaurantsRepository
import com.example.beloo.justeattest.data.repository.restaurant.RestaurantsRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {
    @Binds
    fun restaurantsRepository(repository: RestaurantsRepositoryImpl): RestaurantsRepository
}
