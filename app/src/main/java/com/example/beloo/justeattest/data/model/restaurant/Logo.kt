package com.example.beloo.justeattest.data.model.restaurant

interface Logo {
	val standardUrl: String
}