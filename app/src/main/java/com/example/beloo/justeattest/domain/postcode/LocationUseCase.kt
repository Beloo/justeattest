package com.example.beloo.justeattest.domain.postcode

import android.annotation.SuppressLint
import android.location.Location
import com.example.beloo.justeattest.core.rx.RxNetwork
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.Single.defer
import io.reactivex.subjects.PublishSubject.create
import io.reactivex.subjects.Subject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

interface LocationUseCase {
	/** get user location
	 * this call requires location permission
	 * emits [SecurityException] if permission is not acquired**/
	fun getLocation(): Single<Location>
}

class LocationUseCaseImpl @Inject constructor(
	private val locationProviderClient: FusedLocationProviderClient,
	@RxNetwork
	private val scheduler: Scheduler
) : LocationUseCase {

	@SuppressLint("MissingPermission")
	override fun getLocation(): Single<Location> = defer {
		val subject: Subject<Location> = create()
		val callback: LocationCallback = object : LocationCallback() {
			override fun onLocationResult(result: LocationResult?) {
				result?.lastLocation?.let { subject.onNext(it) } ?: error("location undefined")
			}
		}
		locationProviderClient.requestLocationUpdates(
			LocationRequest().setPriority(LocationRequest.PRIORITY_LOW_POWER),
			callback,
			null)


		return@defer subject
			.subscribeOn(scheduler)
			.firstOrError()
			.timeout(20, TimeUnit.SECONDS)
			.doFinally {
				locationProviderClient.removeLocationUpdates(callback)
			}

	}


}