package com.example.beloo.justeattest.presentation.util.recyclerView

internal interface Bind<in T> {
    fun bindItem(item : T)
    fun unbind()
}