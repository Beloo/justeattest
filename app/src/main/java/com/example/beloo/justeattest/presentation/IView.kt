package com.example.beloo.justeattest.presentation

import android.support.annotation.StringRes

interface IView {
	fun showError(throwable: Throwable)
	fun showError(@StringRes stringResource: Int)
	fun showProgress()
	fun hideProgress()
}
