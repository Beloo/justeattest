package com.example.beloo.justeattest.presentation.restaurant

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.beloo.justeattest.R
import com.example.beloo.justeattest.data.model.restaurant.Restaurant
import com.example.beloo.justeattest.presentation.util.recyclerView.BindViewHolder
import com.example.beloo.justeattest.presentation.util.recyclerView.ViewHolderFactory
import kotlinx.android.synthetic.main.item_restaurant.view.*

class RestaurantViewHolderFactory: ViewHolderFactory {

    override fun createViewHolder(inflater: LayoutInflater, parent: ViewGroup?, viewType: Int): BindViewHolder<*> =
        RestaurantItem(inflater.inflate(R.layout.item_restaurant, parent, false))

}

private class RestaurantItem(view: View): BindViewHolder<Restaurant>(view) {

    private val ratingBar = view.ratingBar
    private val tvName = view.tvName
    private val tvFoodTypes = view.tvFoodTypes
    private val ivLogo = view.ivLogo

    override fun bindItem(item: Restaurant) {
        super.bindItem(item)
        item.logo?.standardUrl?.let {
            Glide.with(ivLogo.context)
                .load(it)
                .crossFade()
                .into(ivLogo)
        }

        tvName.text = item.name
        ratingBar.rating = item.ratingAverage
        var foodTypes = ""
        item.cuisineTypes.forEach {
            foodTypes += "${it.name},"
        }
        foodTypes = foodTypes.removeSuffix(",")
        tvFoodTypes.text = foodTypes
    }


}