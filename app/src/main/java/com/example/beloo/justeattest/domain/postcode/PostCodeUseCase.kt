package com.example.beloo.justeattest.domain.postcode

import android.location.Geocoder
import com.example.beloo.justeattest.core.rx.RxNetwork
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject

interface PostCodeUseCase {
    fun getPostCodeFromLocation(latitude: Double, longitude: Double): Single<String>
}

class PostCodeUseCaseImpl @Inject constructor(
        private val geocoder: Geocoder,
        @RxNetwork
        private val scheduler: Scheduler
) : PostCodeUseCase {

    override fun getPostCodeFromLocation(latitude: Double, longitude: Double): Single<String> = Single.fromCallable {
        val addresses = geocoder.getFromLocation(latitude, longitude, 10)
        val postCode = addresses.firstOrNull {
            it.postalCode != null
        }?.postalCode ?: ""
        postCode
    }.subscribeOn(scheduler)

}