package com.example.beloo.justeattest.core

import android.content.Context
import android.location.Geocoder
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides

@Module
class LocationModule {

    @Provides
    fun provideGeocoder(context: Context): Geocoder = Geocoder(context)

    @Provides
    fun provideLocationProvider(context: Context): FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
}