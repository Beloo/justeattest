package com.example.beloo.justeattest.network

import com.example.beloo.justeattest.network.restaurant.RestaurantRemoteSource
import com.example.beloo.justeattest.network.restaurant.RestaurantsSource
import dagger.Binds
import dagger.Module

@Module
internal interface RemoteDataSourceModule {

	@Binds
	fun provideRestaurantsSource(restaurantRemoteSource: RestaurantRemoteSource): RestaurantsSource

}
