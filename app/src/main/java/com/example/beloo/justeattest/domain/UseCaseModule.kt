package com.example.beloo.justeattest.domain

import com.example.beloo.justeattest.domain.postcode.LocationUseCase
import com.example.beloo.justeattest.domain.postcode.LocationUseCaseImpl
import com.example.beloo.justeattest.domain.postcode.PostCodeUseCase
import com.example.beloo.justeattest.domain.postcode.PostCodeUseCaseImpl
import dagger.Binds
import dagger.Module

@Module
interface UseCaseModule {

    @Binds
    fun postCodeUseCase(postCodeUseCase: PostCodeUseCaseImpl): PostCodeUseCase

    @Binds
    fun locationUseCase(locationUseCase: LocationUseCaseImpl): LocationUseCase
}