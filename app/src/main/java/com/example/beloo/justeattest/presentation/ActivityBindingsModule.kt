package com.example.beloo.justeattest.presentation

import com.example.beloo.justeattest.presentation.restaurant.RestaurantsActivity
import com.example.beloo.justeattest.core.injection.ActivityScope
import com.example.beloo.justeattest.presentation.restaurant.RestaurantsPresentationModule
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Module(includes = [(AndroidSupportInjectionModule::class)])
interface ActivityBindingsModule {
	@ActivityScope
	@ContributesAndroidInjector(modules = [RestaurantsPresentationModule::class])
	fun restaurantsActivity(): RestaurantsActivity
}