package com.example.beloo.justeattest.presentation.restaurant

import com.example.beloo.justeattest.data.model.restaurant.Restaurant
import com.example.beloo.justeattest.presentation.IPresenter
import com.example.beloo.justeattest.presentation.IView
import com.karumi.dexter.listener.single.PermissionListener

interface RestaurantsContract {

    @JvmSuppressWildcards
    interface Presenter: IPresenter<View>, PermissionListener {
        fun findUserLocation()
        fun onSearchRestaurants()
    }

    interface View : IView {
        fun setData(restaurants: List<Restaurant>)
        fun setPostCode(postCode: String)
        fun getPostCode(): String?
    }

    interface Router
}