package com.example.beloo.justeattest.network.restaurant

import com.example.beloo.justeattest.data.model.restaurant.Restaurant
import io.reactivex.Single

interface RestaurantsSource {
	fun getRestaurantsByPostCode(postCode: String): Single<List<Restaurant>>
}