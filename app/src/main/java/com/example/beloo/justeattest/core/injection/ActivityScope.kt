package com.example.beloo.justeattest.core.injection

import javax.inject.Scope

@Scope
annotation class ActivityScope
