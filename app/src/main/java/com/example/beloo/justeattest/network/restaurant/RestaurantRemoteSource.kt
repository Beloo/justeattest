package com.example.beloo.justeattest.network.restaurant

import com.example.beloo.justeattest.core.rx.RxNetwork
import com.example.beloo.justeattest.data.model.restaurant.Restaurant
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject

internal class RestaurantRemoteSource @Inject constructor(
	private val repositoryApi: RestaurantApi,
	@RxNetwork
	private val scheduler: Scheduler
) : RestaurantsSource {

	override fun getRestaurantsByPostCode(postCode: String): Single<List<Restaurant>> {
		if (postCode.isEmpty()) error("empty post code")
		return repositoryApi.getUserRepositories(postCode)
				.map { it.restaurants as List<Restaurant> }
				.subscribeOn(scheduler)
	}

}
