package com.example.beloo.justeattest.network.restaurant

import com.example.beloo.justeattest.data.model.restaurant.Cuisine
import com.example.beloo.justeattest.data.model.restaurant.Logo
import com.example.beloo.justeattest.data.model.restaurant.Restaurant
import com.fasterxml.jackson.annotation.JsonProperty

data class RestaurantListPoJo(
    @JsonProperty("Restaurants")
    val restaurants: List<RestaurantPoJo>
)

data class RestaurantPoJo(
    @JsonProperty("Name")
    override val name: String,
    @JsonProperty("CuisineTypes")
    override val cuisineTypes: List<CuisinePoJo>,
    @JsonProperty("Logo")
    private val logos: List<LogoPoJo>,
    @JsonProperty("RatingAverage")
    override val ratingAverage: Float
) : Restaurant {

    override val logo: Logo?
        get() = logos.firstOrNull()
}

data class CuisinePoJo(
    @JsonProperty("Id")
    override val id: String,
    @JsonProperty("Name")
    override val name: String) : Cuisine

data class LogoPoJo(
    @JsonProperty("StandardResolutionURL")
    override val standardUrl: String
) : Logo