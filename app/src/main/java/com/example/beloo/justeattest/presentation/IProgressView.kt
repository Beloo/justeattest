package com.example.beloo.justeattest.presentation

interface IProgressView {
	fun showProgress()
	fun hideProgress()
}
