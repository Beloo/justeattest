package com.example.beloo.justeattest.network

object ServerContract {

    internal object Restaurant {
        const val RESTAURANTS = "/restaurants"
    }
}

object HTTP {
    internal object Headers {
        const val CONTENT_TYPE = "Content-Type"
        const val ACCEPT = "Accept"
        const val ACCEPT_TENANT = "Accept-Tenant"
        const val ACCEPT_LANGUAGE = "Accept-Language"
        const val AUTHORIZATION = "Authorization"
        const val HOST = "Host"
    }

    internal object MimeType {
        const val APPLICATION_JSON = "application/json"
    }
}