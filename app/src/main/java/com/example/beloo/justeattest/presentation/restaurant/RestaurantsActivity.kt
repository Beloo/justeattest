package com.example.beloo.justeattest.presentation.restaurant

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.example.beloo.justeattest.R
import com.example.beloo.justeattest.data.model.restaurant.Restaurant
import com.example.beloo.justeattest.presentation.PresentationActivity
import com.example.beloo.justeattest.presentation.util.recyclerView.FactoryAdapter
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class RestaurantsActivity : PresentationActivity(), RestaurantsContract.View {

    @Inject
    lateinit var presenter: RestaurantsContract.Presenter

    private val itemAdapter: FactoryAdapter<Restaurant> =
        FactoryAdapter(RestaurantViewHolderFactory())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener {
            presenter.findUserLocation()
        }

        btnSearch.setOnClickListener { presenter.onSearchRestaurants() }

        rvList.apply {
            layoutManager = LinearLayoutManager(
                this@RestaurantsActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = itemAdapter
        }
    }

    override fun getPostCode(): String? = etLocation.text.toString()

    override fun setData(restaurants: List<Restaurant>) {
        vEmpty.visibility = if (restaurants.isEmpty()) View.VISIBLE else View.GONE
        itemAdapter.removeAll()
        itemAdapter.addAll(restaurants)
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun setPostCode(postCode: String) {
        etLocation.setText(postCode)
    }
}
