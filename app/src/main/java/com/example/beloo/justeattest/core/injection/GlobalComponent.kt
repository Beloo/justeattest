package com.example.beloo.justeattest.core.injection


import com.example.beloo.justeattest.JustEatTestApplication
import com.example.beloo.justeattest.core.LocationModule
import com.example.beloo.justeattest.core.rx.RxModule
import com.example.beloo.justeattest.data.repository.RepositoryModule
import com.example.beloo.justeattest.domain.UseCaseModule
import com.example.beloo.justeattest.network.JsonConverterModule
import com.example.beloo.justeattest.network.NetworkApiModule
import com.example.beloo.justeattest.network.RemoteDataSourceModule
import com.example.beloo.justeattest.network.restaurant.RestaurantsSource
import com.example.beloo.justeattest.presentation.ActivityBindingsModule
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    (GlobalModule::class),
    (ActivityBindingsModule::class),
    (AndroidSupportInjectionModule::class),
    (NetworkApiModule::class),
    LocationModule::class,
    (JsonConverterModule::class),
    (RemoteDataSourceModule::class),
    (RepositoryModule::class),
    UseCaseModule::class,
    RxModule::class
])
interface GlobalComponent {
    fun repositorySource(): RestaurantsSource

    fun inject(app: JustEatTestApplication)
}
