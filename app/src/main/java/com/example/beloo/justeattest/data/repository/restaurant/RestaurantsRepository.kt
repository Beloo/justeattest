package com.example.beloo.justeattest.data.repository.restaurant

import com.example.beloo.justeattest.data.model.restaurant.Restaurant
import com.example.beloo.justeattest.network.restaurant.RestaurantsSource
import io.reactivex.Single
import javax.inject.Inject

interface RestaurantsRepository {
    fun getRestaurantsByPostCode(postCode: String): Single<List<Restaurant>>
}

class RestaurantsRepositoryImpl @Inject constructor(
        r: RestaurantsSource
): RestaurantsRepository, RestaurantsSource by r