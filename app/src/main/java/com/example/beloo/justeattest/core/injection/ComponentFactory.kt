package com.example.beloo.justeattest.core.injection

import android.content.Context

interface ComponentFactory {

    val appContext: Context

    fun globalComponent(): GlobalComponent

}
