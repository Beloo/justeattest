package com.example.beloo.justeattest.presentation.restaurant

import android.Manifest
import com.example.beloo.justeattest.presentation.ActivityCommonModule
import com.example.beloo.justeattest.presentation.IPresenter
import com.example.beloo.justeattest.presentation.IView
import com.example.beloo.justeattest.presentation.PresentationActivity
import com.karumi.dexter.DexterBuilder
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module(includes = [
    ActivityCommonModule::class,
    RestaurantsPresentationModule.ProvideModule::class])
interface RestaurantsPresentationModule {

    @Binds
    fun provideActivity(activity: RestaurantsActivity): PresentationActivity

    @Binds
    fun provideView(activity: RestaurantsActivity): RestaurantsContract.View


    @Binds
    fun presenter(presenter: RestaurantsPresenter): RestaurantsContract.Presenter

    @Module
    class ProvideModule {

        @Provides
        fun provideLocationPermissionListener(dexter: DexterBuilder.Permission, listener: RestaurantsContract.Presenter): DexterBuilder = dexter
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(listener)

        @Provides
        @JvmSuppressWildcards
        fun basePresenter(presenter: RestaurantsContract.Presenter): IPresenter<IView> = presenter as IPresenter<IView>
    }
}
