package com.example.beloo.justeattest

import android.app.Activity
import android.app.Application

import com.example.beloo.justeattest.core.injection.ComponentCreator
import com.example.beloo.justeattest.core.injection.ComponentFactory
import com.example.beloo.justeattest.core.injection.ComponentProvider
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject


class JustEatTestApplication : Application(), HasActivityInjector {

	@Inject
	internal lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

	override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

	private lateinit var componentFactory: ComponentFactory

	override fun onCreate() {
		super.onCreate()

		instance = this
		componentFactory = ComponentProvider(ComponentCreator(this))
		componentFactory().globalComponent().inject(this)
	}

	fun componentFactory(): ComponentFactory = componentFactory

	companion object {
		private lateinit var instance: JustEatTestApplication
		fun instance(): JustEatTestApplication = instance
	}
}
