package com.example.beloo.justeattest.presentation.restaurant

import com.example.beloo.justeattest.R
import com.example.beloo.justeattest.core.injection.ActivityScope
import com.example.beloo.justeattest.data.repository.restaurant.RestaurantsRepository
import com.example.beloo.justeattest.domain.postcode.LocationUseCase
import com.example.beloo.justeattest.domain.postcode.PostCodeUseCase
import com.example.beloo.justeattest.presentation.AbstractPresenter
import com.example.beloo.justeattest.presentation.SubscriptionCache
import com.karumi.dexter.DexterBuilder
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import dagger.Lazy
import javax.inject.Inject

@ActivityScope
class RestaurantsPresenter @Inject constructor(
	private val repository: RestaurantsRepository,
	private val locationUseCase: LocationUseCase,
	private val postCodeUseCase: PostCodeUseCase,
	private val locationPermission: Lazy<DexterBuilder>,
	cache: SubscriptionCache
) : AbstractPresenter<RestaurantsContract.View>(cache), RestaurantsContract.Presenter {

	companion object {
		private const val TAG_POSTCODE = "postcode"
		private const val TAG_RESTAURANTS = "restaurants"
	}

	override fun onPermissionDenied(response: PermissionDeniedResponse?) {
		view.showError(R.string.error_need_location_permission)
	}

	override fun onPermissionRationaleShouldBeShown(
		permission: PermissionRequest?,
		token: PermissionToken?
	) {
	}


	override fun onPermissionGranted(response: PermissionGrantedResponse?) {
		getRestaurantsByLocation()
	}

	private fun getRestaurantsByLocation() {
		if (isSubscribed(TAG_POSTCODE)) return
		locationUseCase.getLocation().flatMap {
			postCodeUseCase.getPostCodeFromLocation(it.latitude, it.longitude)
		}.subscribeManaged(
			TAG_POSTCODE,
			{
				view.setPostCode(it)
				getRestaurantsByPostCode(it)
			},
			{ view.showError(it) }
		)
	}
	private fun getRestaurantsByPostCode(postCode: String) {
		if (isSubscribed(TAG_RESTAURANTS)) return
		view.showProgress()
		repository.getRestaurantsByPostCode(postCode)
			.subscribeManaged(
				TAG_RESTAURANTS,
				{
					view.hideProgress()
					view.setData(it)
				},
				{
					view.hideProgress()
					view.showError(it)
				})
	}

	override fun onReady() {
		val postCode: String? = view.getPostCode()
		if (!postCode.isNullOrEmpty()) {
			getRestaurantsByPostCode(postCode!!)
		}
	}

	override fun onSearchRestaurants() {
		val postCode: String? = view.getPostCode()
		if (postCode.isNullOrEmpty()) {
			view.showError(R.string.error_enter_post_code)
			return
		}
		getRestaurantsByPostCode(postCode!!)
	}

	override fun findUserLocation() {
		locationPermission.get().check()
	}

}

