package com.example.beloo.justeattest.network.restaurant

import com.example.beloo.justeattest.network.ServerContract
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RestaurantApi {

    /** get list of all user repositories */
    @GET(ServerContract.Restaurant.RESTAURANTS)
    fun getUserRepositories(@Query("q") postCode: String): Single<RestaurantListPoJo>

}
