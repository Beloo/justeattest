package com.example.beloo.justeattest.presentation

interface IPresenter<View : IView> {
	val isViewAttached: Boolean
	fun bindView(view: View)
	fun unbindView()

	val view: View
}
