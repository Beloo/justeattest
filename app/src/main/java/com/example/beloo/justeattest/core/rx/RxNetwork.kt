package com.example.beloo.justeattest.core.rx

import javax.inject.Qualifier

@Qualifier
annotation class RxNetwork
