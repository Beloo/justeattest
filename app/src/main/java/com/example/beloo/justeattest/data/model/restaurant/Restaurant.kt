package com.example.beloo.justeattest.data.model.restaurant

interface Restaurant {
	val name: String
	val cuisineTypes: List<Cuisine>
	val logo: Logo?
	val ratingAverage: Float
}