package com.example.beloo.justeattest.data.model.restaurant

interface Cuisine {
	val id: String
	val name: String
}